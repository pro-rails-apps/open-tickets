OpenTickets
===========

Introduction
------------

[This software uses [Semantic Versioning](http://semver.org/) (semver)]

OpenTickets is a Ruby on Rails solution for Event Ticketing inspired by the Community Edition of Wordpress Plugin - [OpenTickets](http://opentickets.com/community-edition/).

The OpenTickets community edition Wordpress Plugin is integrated with the famous Wordpress WooCommerce Plugin

OpenTickets is standalone Ticketing Solution unlike the Wordpress Plugin which is installed in the Wordpress.

Currently being developed by [Agnel Waghela](https://agnelwaghela.in/).
Any suggestions are welcome and contributions appreciated.

Contributing
------------

We appreciate all contributions! If you would like to contribute, please follow these steps:

* Fork the repo.
* Create a branch with a name that describes the change.
* Make your changes in the branch.
* Submit a pull-request to merge your feature-branch in our master branch.

License
-------

OpenTickets is licensed under GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007